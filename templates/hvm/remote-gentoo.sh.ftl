#!/bin/bash

echo 'Partition ${device}'

fdisk ${device} << EOF
n
p
1


w
EOF

echo "Format volume"
echo 'mkfs -t ${rootfstype} ${device}1'
mkfs -t ${rootfstype} ${device}1

echo "Create mount point"
echo 'mkdir -p ${mountPoint}'
mkdir -p ${mountPoint}

echo "Mount volume"
echo 'mount ${device}1 ${mountPoint}'
mount ${device}1 ${mountPoint}

echo "Download stage3"
<#if architecture == "i386">
    <#assign archDir = "x86">
    <#assign archFile = "i686">
<#else>
    <#assign archDir = "amd64">
    <#assign archFile = "amd64">
</#if>
#curl --silent -o /tmp/stage3.tar.bz2 "${mirror}releases/${archDir}/autobuilds/`curl --silent "${mirror}releases/${archDir}/autobuilds/latest-stage3-${archFile}.txt" | grep stage3-${archFile}`"
echo 'curl --silent -o /tmp/stage3.tar.bz2 "${mirror}releases/${archDir}/autobuilds/`curl --silent "${mirror}releases/${archDir}/autobuilds/latest-stage3-${archFile}.txt" | grep stage3-${archFile} | awk "{ print \$1 }"`"'
curl --silent -o /tmp/stage3.tar.bz2 "${mirror}releases/${archDir}/autobuilds/`curl --silent "${mirror}releases/${archDir}/autobuilds/latest-stage3-${archFile}.txt" | grep stage3-${archFile} | awk "{ print \$1 }"`"

echo "Download portage"
echo 'curl --silent -o /tmp/portage.tar.bz2 "${mirror}snapshots/portage-latest.tar.bz2"'
curl --silent -o /tmp/portage.tar.bz2 "${mirror}snapshots/portage-latest.tar.bz2"

echo "Unpack stage3"
echo 'tar -xjpf /tmp/stage3.tar.bz2 -C ${mountPoint}'
tar -xjpf /tmp/stage3.tar.bz2 -C ${mountPoint}

echo "Unpack portage"
echo 'tar -xjf /tmp/portage.tar.bz2 -C ${mountPoint}/usr'
tar -xjf /tmp/portage.tar.bz2 -C ${mountPoint}/usr

echo "Setup files"

echo "/etc/resolv.conf"
echo 'cp -L /etc/resolv.conf ${mountPoint}/etc/resolv.conf'
cp -L /etc/resolv.conf ${mountPoint}/etc/resolv.conf

echo "/tmp/build.sh"
cat <<'END_OF_FILE'>${mountPoint}/tmp/build.sh
<#include "/tmp/build.sh.ftl">
END_OF_FILE
chmod 755 ${mountPoint}/tmp/build.sh

echo "Mount /dev"

echo 'mount -t proc none ${mountPoint}/proc'
mount -t proc none ${mountPoint}/proc
echo 'mount --rbind /dev ${mountPoint}/dev'
mount --rbind /dev ${mountPoint}/dev
echo 'mount --rbind /dev/pts ${mountPoint}/dev/pts'
mount --rbind /dev/pts ${mountPoint}/dev/pts

echo "Run build script"

echo 'chroot ${mountPoint} /tmp/build.sh'
chroot ${mountPoint} /tmp/build.sh

echo "Cleanup"

echo 'rm -fR ${mountPoint}/tmp/*'
rm -fR ${mountPoint}/tmp/*
echo 'rm -fR ${mountPoint}/var/tmp/*'
rm -fR ${mountPoint}/var/tmp/*
echo 'rm -fR ${mountPoint}/usr/portage/distfiles/*'
rm -fR ${mountPoint}/usr/portage/distfiles/*

echo 'shutdown -h now'
shutdown -h now

