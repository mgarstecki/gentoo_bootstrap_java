#!/bin/bash

[ -f gentoo-console-bootstrap/target/gentoo-console-bootstrap.jar ] || mvn clean package

java \
-Darchaius.configurationSource.additionalUrls=http://public.dowdandassociates.com/products/gentoo_bootstrap_java/raw/master/config/Pygoscelis-Papua-HVM_us-east-1.properties \
-Dlog4j.configuration=http://public.dowdandassociates.com/logging-configuration/blitz4j/log4j-info-stdout.properties \
-Dcom.dowdandassociates.gentoo.bootstrap.mirror=http://gentoo.osuosl.org/ \
-jar gentoo-console-bootstrap/target/gentoo-console-bootstrap.jar

